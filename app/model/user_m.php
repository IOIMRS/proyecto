<?php
namespace app\model;

class user_m extends \framework\lib\model{

    function __construct()
    {
        parent::__construct();
        $this->table="user";
        $this->columns=["id_user","email","password","contact_number","user_name","token","roles_Id_rol"];


        $this->manyToOne=[
            "roles"=>["roles_Id_rol","Id_rol","token"]

        ];

    }


}