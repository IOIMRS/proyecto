<?php   
use web\web as web;
class AtributoTest extends \PHPUnit\framework\TestCase{
    
    public function testAtributoBorrar(){
        

        
        \web\web::registrarRutas("atribute/remove","Atributo","borrar");
        $_GET['id']=1;
        web::validarArchivos('atribute/remove');

        $respuesta=(array)json_decode(ob_get_contents());
        
        $this->assertEquals(1,$respuesta['code']);

        

        
        
    }
    public function testAtributoListar(){
        

        
        \web\web::registrarRutas("atribute/listar","Atributo","listar");
        web::validarArchivos('atribute/listar');

        $respuesta=(array)json_decode(ob_get_contents());
        
        $this->assertEquals(1,$respuesta['code']); // it only needs the code rather than the message or to verify if it returns data. 
        
        

        
        
    }
} 
?>