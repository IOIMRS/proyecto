<?php
use web\web as web;
class proveedorTest extends \PHPUnit\framework\TestCase{
    
    public function testproveedorBorrar(){
        

        //registro de ruta
        \web\web::registrarRutas("proveedor/borrar","proveedor","borrar"); 
        //Se envia id del registro
        $_GET['id']=1;
        //Se llama al controlador
        web::validarArchivos('proveedor/borrar');
        //Se captura respuesta de buffer
        $respuesta=(array)json_decode(ob_get_contents());
        $this->assertEquals(1,$respuesta['code']);

        

        
        
    }
    public function testProveedorListar(){
        

        
        \web\web::registrarRutas("proveedor/listar","proveedor","listar");
        web::validarArchivos('proveedor/listar');

        $respuesta=(array)json_decode(ob_get_contents());
        
        $this->assertEquals(1,$respuesta['code']); // it only needs the code rather than the message or to verify if it returns data. 
        
        

        
        
    }
} 
?>