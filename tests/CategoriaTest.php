<?php
use web\web as web;
class CategoriaTest extends \PHPUnit\framework\TestCase{
    
    public function testCategoriaBorrar(){
        

        
        \web\web::registrarRutas("categoria/remove","categoria","borrar");
        $_GET['id']=2;
        web::validarArchivos('categoria/remove');

        $respuesta=(array)json_decode(ob_get_contents());
        $this->assertEquals(1,$respuesta['code']);

        

        
        
    }

    public function testCategoriaListar(){
        \web\web::registrarRutas("categoria/listar","categoria","listar");
        web::validarArchivos('categoria/listar');

        $respuesta=(array)json_decode(ob_get_contents());
        // it only needs the code rather than the message or to verify if it returns data.
        $this->assertEquals(1,$respuesta['code']); 
    }
} 
?>